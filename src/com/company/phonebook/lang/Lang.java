package com.company.phonebook.lang;

public abstract class Lang {
    public static Lang instance;

    public abstract String langName();
    public abstract String[] scheduleOptions();
    public abstract String startMessage();
    public abstract String optionInvalid();
    public abstract String optionsMessage();
    public abstract String enterName();
    public abstract String enterNumber();
    public abstract String contactAdd();
    public abstract String enterNameOrNumber();
    public abstract String contactNone();
    public abstract String sureRemoveContact();
    public abstract String sureRemoveContactPositive();
}
