package com.company.phonebook.lang;

public class PtBR extends Lang {
    public static Lang instance = new PtBR();

    @Override
    public String langName() {
        return "Portugues Brasil";
    }

    @Override
    public String[] scheduleOptions() {
        return new String[]{"Consultar","Listar","Adicionar","Remover","Sair"};
    }

    @Override
    public String startMessage() {
        return "Agenda de contatos:";
    }

    @Override
    public String optionInvalid() {
        return "Opção invalida!";
    }

    @Override
    public String optionsMessage() {
        return "O que deseja fazer?";
    }

    @Override
    public String enterName() {
        return "Insira o nome:";
    }

    @Override
    public String enterNumber() {
        return "Insira o numero:";
    }

    @Override
    public String contactAdd() {
        return "Contato adicionado";
    }

    @Override
    public String enterNameOrNumber() {
        return "Insira o nome ou numero";
    }

    @Override
    public String contactNone() {
        return "Nenhum contato encontrado";
    }

    @Override
    public String sureRemoveContact() {
        return "Tem certeza que deseja remover este contato?";
    }

    @Override
    public String sureRemoveContactPositive() {
        return "sim";
    }
}
