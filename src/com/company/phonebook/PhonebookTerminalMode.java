package com.company.phonebook;

import com.company.phonebook.lang.Lang;
import com.company.phonebook.lang.PtBR;

import java.util.ArrayList;
import java.util.Scanner;

public class PhonebookTerminalMode {
    private Phonebook phonebook;
    private Lang lang;
    Scanner in;

    public PhonebookTerminalMode(Phonebook phonebook, Lang lang){
        this.phonebook = phonebook;
        this.lang = lang;
        in = new Scanner(System.in);
    }

    public PhonebookTerminalMode(Phonebook phonebook) {
        this.phonebook = phonebook;
        this.lang = PtBR.instance;
        in = new Scanner(System.in);
    }

    public void run() {
        System.out.println(lang.startMessage());

        int itemSelect = 0;

        String[] scheduleOptions = lang.scheduleOptions();
        while (true) {
            System.out.println(lang.optionsMessage());
            System.out.println();
            int itemIndex = 1;
            for (String item : scheduleOptions) {
                System.out.println(itemIndex++ + "-" + item);
            }
            String resp = in.nextLine();
            try {
                itemSelect = Integer.parseInt(resp);
            } catch (Exception e) {
                itemSelect = Integer.MAX_VALUE;
            }
            if (itemSelect <= 0 || itemSelect > scheduleOptions.length) {
                System.out.println(lang.optionInvalid());
                System.out.println();
                continue;
            }

            switch (itemSelect) {
                case 1:
                    find();
                    break;
                case 2:
                    list();
                    break;
                case 3:
                    add();
                    break;
                case 4:
                    remove();
                    break;
                case 5:
                    return;
            }
        }

    }


    public void add(){
        System.out.println(lang.enterName());
        String name = in.nextLine();
        System.out.println(lang.enterNumber());
        String number = in.nextLine();
        Contact contact = new Contact(name,number);
        phonebook.add(contact);
        System.out.println(lang.contactAdd());
        System.out.println(name + " - " + number);
    }


    public void remove(){
        Contact contact = null;
        do {
            System.out.println(lang.enterNameOrNumber());
            String text = in.nextLine();
            if (text.equals("")) {
                return;
            }
            try {
                contact = phonebook.find(text).get(0);
            } catch (Exception e) {
                System.out.println(lang.contactNone());
                contact = null;
            }
        }while(contact == null);

        System.out.println(lang.sureRemoveContact());
        System.out.println(lang.sureRemoveContactPositive());
        System.out.println(contact.getName() + " - " + contact.getNumber());
        String text = in.nextLine();
        if(text.equals(lang.sureRemoveContactPositive())){
            phonebook.remove(contact);
        }
    }

    public void find(){
        do {
            System.out.println(lang.enterNameOrNumber());
            String text = in.nextLine();
            if (text.equals("")) {
                return;
            }
            ArrayList<Contact> contacts = phonebook.find(text);
            for(Contact c : contacts){
                System.out.println(c.getName() + " - "+ c.getNumber());
            }
        }while(true);
    }

    public void list(){
        for(Contact c : phonebook.getContacts()){
            System.out.println(c.getName() + " - "+ c.getNumber());
        }
    }
}
