package com.company.phonebook;

import java.util.*;

public class Phonebook {
    private ArrayList<Contact> contacts;

    public Phonebook(){
        contacts = new ArrayList<Contact>();
    }

    public void add(Contact contact){
        contacts.add(contact);
    }

    public void remove(Contact contact){
        contacts.remove(contact);
    }

    public Contact find(Contact contact){
        for (Contact c:contacts) {
            if(contact.equals(c))
                return contact;
        }
        return null;
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public ArrayList<Contact>  find(String text){
        ArrayList<Contact> contactsFound = new ArrayList<Contact>();
        for (Contact c:contacts)
            if(c.contains(text))
                contactsFound.add(c);
        return contactsFound;
    }
}
