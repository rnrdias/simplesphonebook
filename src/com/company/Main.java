package com.company;

import com.company.phonebook.Phonebook;
import com.company.phonebook.PhonebookTerminalMode;

public class Main {

    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();
        PhonebookTerminalMode stm = new PhonebookTerminalMode(phonebook);
        stm.run();
    }
}
